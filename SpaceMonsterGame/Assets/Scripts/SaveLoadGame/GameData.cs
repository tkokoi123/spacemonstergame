using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData 
{
    public  int Point { get; set; } = 0;
    public int SpaceCraftHealthPoint { get; set; } = 3;
    public string Amo { get; set; } = "DestroyAmmo1";
    
    public float AmoLevel { get; set; } = 1;
    public string GameLevel { get; set; } = "Level1";

}
