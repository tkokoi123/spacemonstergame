using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    public static void SaveGame(GameData gameData)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "SpaceCraft.fun";
        FileStream stream = new FileStream(path, FileMode.Create);

        formatter.Serialize(stream, gameData);  
        stream.Close();
    }
    public static GameData LoadGame()
    {
        string path = Application.persistentDataPath + "SpaceCraft.fun";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            GameData data = formatter.Deserialize(stream) as GameData;
            stream.Close();
            return data;
        }
        else
        {
            Debug.LogError("Save file not found in " + path);
            return null;
        }
    }
    public static void RemoveFile()
    {
        string path = Application.persistentDataPath + "SpaceCraft.fun";
        if (File.Exists(path))
        {
            File.Delete(path);
        }
    }
}
