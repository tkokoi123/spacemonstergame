using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy2_2Move : MonoBehaviour
{
    const float MinImpulseForce = 2f;
    const float MaxImpulseForce = 4f;
    // Start is called before the first frame update
    void Start()
    {
        
        float angle = Random.Range(0, 2 * Mathf.PI);
        if (angle > 0)
        {
            angle = -angle;
        }
        var cos = Mathf.Cos(angle);
        var sin = Mathf.Sin(angle);
        if (sin > 0)
        {
            sin = -sin;
        }
        var position = Camera.main.ScreenToWorldPoint(transform.position);
        if (position.x < 0) cos = Mathf.Abs(cos);
        if (position.x > 0) cos = -Mathf.Abs(cos);
        Vector2 direction = new Vector2(cos, sin);
        float magnitude = Random.Range(MinImpulseForce, MaxImpulseForce);
        GetComponent<Rigidbody2D>().AddForce(direction * magnitude, ForceMode2D.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
