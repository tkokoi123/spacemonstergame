using Assets.Script;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy2_1 : MonoBehaviour
{
    [SerializeField]
    public GameObject explode;
    public int Health { get; set; }
    [SerializeField]
    public List<GameObject> Rewards;
    void Start()
    {
        Health = 3;
    }

    
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Contains("DestroyAmmo"))
        {
            if (collision.gameObject.tag.Contains("DestroyAmmo")) 
                Health -= collision.gameObject.GetComponent<AmmoManage>().Damage;

            if (Health <= 0)
            {
                Instantiate(explode, transform.position, Quaternion.identity);
                AudiosManager.Instance.PlaySFX("bomb");
                Destroy(gameObject);

                //spawn a reward
                var chance = Random.Range(1,11);
                if (chance >= 1 && chance <=7)
                {
                    SpawnReward(Rewards);
                }
                SpaceCraft.Point++;
                CommonFunction.SetTextPoint(SpaceCraft.Point);
            }
        }
    }

    public void SpawnReward(List<GameObject> list)
    {
        var item = RandomGameObject(list);
        Instantiate(item, transform.position, Quaternion.identity);
    }
    public GameObject RandomGameObject(List<GameObject> list)
    {
        int index = Random.Range(0, list.Count);
        return list[index];
    }
}
