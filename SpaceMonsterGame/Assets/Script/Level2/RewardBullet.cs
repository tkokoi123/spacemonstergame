using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardBullet : MonoBehaviour
{
    //loai dan
    public int bulletType;
    public GameObject ammo;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.Equals("maybay2d_0"))
        {
            collision.gameObject.GetComponent<Shoot>().SetAmmo(ammo);
            SpaceCraft.Point++;
            CommonFunction.SetTextPoint(SpaceCraft.Point);
            Destroy(gameObject);
        }
    }
}
