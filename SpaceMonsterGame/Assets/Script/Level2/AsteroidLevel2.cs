using Assets.Script;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidLevel2 : MonoBehaviour
{
    const float Impulse = 4f;
    Rigidbody2D rb;
    public static int Damage = 1;
    int Health;
    [SerializeField]
    GameObject Explosion;
    // Start is called before the first frame update
    void Start()
    {
        Health = 2;
        rb = GetComponent<Rigidbody2D>();
        var ship = GameObject.Find("maybay2d_0");
        if(ship != null)
        {
        Vector2 direction = new Vector2(ship.transform.position.x - transform.position.x, ship.transform.position.y - transform.position.y);
        direction.Normalize();
        rb.AddForce(direction * Impulse, ForceMode2D.Impulse);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.Equals("maybay2d_0"))
        {
            Instantiate(Explosion, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
        if (collision.gameObject.tag.Contains("DestroyAmmo"))
        {
            Health -= collision.gameObject.GetComponent<AmmoManage>().Damage;
            if (Health <= 0)
            {
                Instantiate(Explosion, transform.position, Quaternion.identity);
                Destroy(gameObject);
            }   
        }
    }
}
