using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Destroy();
    }

    public void Move(GameObject obj, Vector3 dir, float speed)
    {
        obj.transform.Translate((speed * Time.deltaTime * dir));
    }

    public void Destroy()
    {
        Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
        if (screenPosition.y > Screen.height || screenPosition.y < 0 || screenPosition.x > Screen.width || screenPosition.x<0)
        {
            Destroy(this.gameObject);
        }
            
    }
}
