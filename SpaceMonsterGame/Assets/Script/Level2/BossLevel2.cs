using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class BossLevel2 : MonoBehaviour
{
    // Start is called before the first frame update
    public List<GameObject> ListPos;
    public GameObject Bullet;
    internal int health;
    float speed;
    int randomIndex;
    internal int numOfBullet;
    public GameObject Explosion;
    public GameObject ExplosionBoss;
    int frameCount;
    private void Awake()
    {
        
    }
    void Start()
    {
        frameCount = 0;
        ListPos = GameObject.FindGameObjectsWithTag("ListPos").ToList();
        Debug.Log("Start BossLevel2");
        numOfBullet = 0;
        speed = 3.0f;
        health = 100;
        randomIndex = Random.Range(0, ListPos.Count);
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(frameCount);
        frameCount++;
        if (frameCount % 100 == 0)
        {
            randomIndex = Random.Range(0, ListPos.Count);
        }
        Move();
        if (frameCount % 90 == 0 && Time.timeScale != 0)
        {
            Shoot();
        }
        if (health <= 0)
        {

            Debug.Log("Kill");
            Instantiate(ExplosionBoss, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
            GenerateEnemy.endBossLv5 = true;
            Destroy(this.gameObject);
        }
    }

    public void Move()
    {
        
        float step = speed * Time.deltaTime;
        var target = ListPos.ElementAt(randomIndex).transform.position;
        this.transform.position = Vector2.MoveTowards(this.transform.position, target, step);
    }

    public void Shoot()
    {
        numOfBullet++;
        GameObject instance = Instantiate(Bullet, new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z), Quaternion.identity);
        instance.AddComponent(typeof(Bullet));
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("DestroyAmmo1"))
        {
            health -= ShipAmmo1.Damage+5;
            Instantiate(Explosion, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
            Debug.Log("Hit");
            Destroy(collision.gameObject);
        }
        if ( collision.CompareTag("DestroyAmmo2"))
        {
            health -= ShipAmmo2.Damage+5;
            Instantiate(Explosion, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
            Debug.Log("Hit");
            Destroy(collision.gameObject);
        }
    }
}
