using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewardHeal : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.Equals("maybay2d_0"))
        {
            SpaceCraft.HealthPoint++;
            Text healthPoint = GameObject.Find("HealthPoint").GetComponent<Text>();
            healthPoint.text = $"x{SpaceCraft.HealthPoint}";
            Destroy(gameObject);
        }
    }

}
