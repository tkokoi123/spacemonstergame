using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossLv2Bullet : Bullet
{
    public GameObject SpreadBullettPrefabs;
    public AudioClip spreadSound;
    public AudioClip bulletSound;
    Vector3 initPos;
    bool isSpread;
    float speed;
    int numOfSpreadBullet;
    int frequencySpread;
    BossLevel2 bossLv2;
    AudioSource soundBossBullet;
    // Start is called before the first frame update
    void Start()
    {
        bossLv2 = FindObjectOfType<BossLevel2>();
        soundBossBullet= GetComponent<AudioSource>();
        soundBossBullet.PlayOneShot(bulletSound);
        frequencySpread = 5;
        numOfSpreadBullet = 8;
        Debug.Log("Start Boss Bullet");
        isSpread = true;
        speed = 5f;
        initPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Move(this.gameObject,new Vector3(0,-1,0),speed);
        Spread();
    }
    void Spread()
    {
        if(bossLv2.numOfBullet % frequencySpread == 0)
        {
            if (initPos.y - transform.position.y >= 2.5f)
            {
                if (isSpread)
                {
                    
                    for (int i = 0; i < numOfSpreadBullet; i++)
                    {
                        var minibullet = Instantiate(SpreadBullettPrefabs, new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z), transform.rotation);
                        BossLv2SpreadBullet component = minibullet.GetComponent<BossLv2SpreadBullet>();
                        component.dir = CalculateVectorRotation(i * 360 / numOfSpreadBullet);
                        component.rotation = new Vector3(0, 0, -i * (360 / numOfSpreadBullet));
                    }
                    soundBossBullet.PlayOneShot(spreadSound);
                    this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
                    Destroy(this.gameObject,0.5f);
                    isSpread = false;
                }
            }
        }
        else
        {
            this.Destroy();
        }
        
    }
    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if (collision.CompareTag("DestroySpaceCraft"))
    //    {
    //        Debug.Log("bullet hit");
    //    }
    //}
    Vector3 CalculateVectorRotation(float angle)
    {
        float x = Mathf.Sin(angle * Mathf.Deg2Rad);
        float y = Mathf.Cos(angle * Mathf.Deg2Rad);

        return new Vector3(x, y);
    }
}
