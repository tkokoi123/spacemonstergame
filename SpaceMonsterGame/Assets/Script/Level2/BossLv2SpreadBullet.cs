using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossLv2SpreadBullet : Bullet
{
    // Start is called before the first frame update
    float speed;
    internal Vector3 dir;
    internal Vector3 rotation;
    void Start()
    {
        speed = 5f;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += ((speed * Time.deltaTime * dir));
        //var tmp = transform.position;
        //tmp.z = rotation.z;
        transform.eulerAngles = rotation;
        this.Destroy();
    }
    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if (collision.CompareTag("DestroySpaceCraft"))
    //    {
    //        Debug.Log("Spread bullet hit");
    //    }
    //}
}
