using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScript : MonoBehaviour
{
    public float speed;
    [SerializeField]
    public Renderer bgRenderer;

    //background
    [SerializeField]
    List<Material> Background;

    public static int MaterialIndex;
    
    // Start is called before the first frame update
    void Start()
    {
        MaterialIndex = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (MaterialIndex != -1)
        {
            ChangeMaterial(Background, MaterialIndex);
            MaterialIndex = -1;
        }
        bgRenderer.material.mainTextureOffset += new Vector2(0, speed * Time.deltaTime);
    }
    public void ChangeMaterial(List<Material> material,int materialIndex) {
        bgRenderer.GetComponent<Renderer>().material = material[materialIndex];
    }
}
