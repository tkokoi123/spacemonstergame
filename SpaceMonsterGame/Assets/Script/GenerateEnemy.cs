using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenerateEnemy : MonoBehaviour
{
    public static bool endBossLv5 = false;
    public Transform location;
    [SerializeField]
    public List<GameObject> listEnemy;
    [SerializeField]
    public List<GameObject> listEnemyLevel1;
    [SerializeField]
    public List<GameObject> listEnemyLevel2;
    [SerializeField]
    public List<GameObject> listEnemyLevel4;
    [SerializeField]
    public List<GameObject> listEnemyLevel5;
    [SerializeField]
    public List<GameObject> listAsteroidLevel5;
    [SerializeField]
    public List<GameObject> listItem;
    [SerializeField]
    public float speed;

    //[SerializeField]
    //float secondSpawn = 0.5f;
    [SerializeField]
    float minTras;
    [SerializeField]
    float maxTras;

    public float delay = 5;
    float timer;

    public static string Level { get; set; } = "Level1";
    public static int LevelNumber { get; set; } = 1;
    bool onetime = false;

    //time gen item 
    public float delayItem;
    public float timerItem;
    // Start is called before the first frame update
    void Start()
    {
       
    }
    public void SpawnItem()
    {
        timerItem += Time.deltaTime;
        if (timerItem > delayItem) { 
        SpawnEnemy(listItem);
            timerItem = 0;
        }
    }
    // Update is called once per frame
    public void NextLevel() {
        LevelNumber++;
        Level = "Level"+ LevelNumber;
        onetime = false;
    }
    void Update()
    {
        switch (Level)
        {
            case "Level1":
                delayItem = 20;
                SpawnItem();
                if (!onetime)
                {
                    DisplayLevelText();
                    Invoke("NextLevel", 30);
                }
                timer += Time.deltaTime;
                if (timer > 2)
                {
                    SpawnEnemy(listEnemyLevel1);
                    timer = 0;
                }
                break;
            case "Level2":
                delayItem = 30;
                SpawnItem();
                if (!onetime)
                {
                    SaveSpaceMonsterGame();
                    DisplayLevelText();
                    Invoke("NextLevel", 30);
                }
                timer += Time.deltaTime;
                if (timer > 3)
                {
                    SpawnEnemy(listEnemy);
                    SpawnEnemy(listEnemyLevel2);
                    timer = 0;
                }
                break;
            case "Level3":
                delayItem = 60;
                SpawnItem();
                if (!onetime)
                {
                    
                    SaveSpaceMonsterGame();
                    BackgroundScript.MaterialIndex = 1;
                    DisplayLevelText();
                    Invoke("NextLevel", 30);
                }
                timer += Time.deltaTime;
                if (timer > 3)
                {
                    SpawnEnemy(listEnemyLevel4);
                    timer = 0;
                }
                break;
            case "Level4":
                delayItem = 60;
                SpawnItem();
                if (!onetime)
                {
                    SaveSpaceMonsterGame();
                    DisplayLevelText();
                    Invoke("NextLevel", 30);
                }
                timer += Time.deltaTime;
                if (timer >= 3)
                {
                    
                    SpawnEnemy(listEnemyLevel4);
                    SpawnEnemy(listEnemyLevel5);
                    SpawnEnemy(listAsteroidLevel5);
                    timer = 0;
                }
                break;
            case "Level5":
                delayItem = 30;
                SpawnItem();
                timer += Time.deltaTime;
                if (!onetime)
                {
                    GameController.OnChangHpExist = true;
                    SaveSpaceMonsterGame();
                    DisplayLevelText();
                    var c = GameObject.FindGameObjectWithTag("BossLv2Obj").GetComponent<CommonFunction>();
                    Debug.Log(c);
                    c.SpawnEnemy();
                }
                if (timer >= 3)
                {
                    SpawnEnemy(listEnemyLevel4);
                    SpawnEnemy(listAsteroidLevel5);
                    timer = 0;
                }
                Debug.Log("-----" + endBossLv5);
                if (endBossLv5) 
                {
                    Invoke("NextLevel", 3);
                    endBossLv5 = false;
                }
                break;
            case "Level6":
                delayItem = 20;
                SpawnItem();
                if (!onetime)
                {
                    SaveSpaceMonsterGame();
                    BackgroundScript.MaterialIndex = 2;
                    DisplayLevelText();
                    var c = GameObject.FindGameObjectWithTag("CommonFunction").GetComponent<CommonFunction>();
                    c.SpawnEnemy();
                }
                timer += Time.deltaTime;
                if (timer > delay)
                {
                    SpawnEnemy(listEnemyLevel5);
                    timer = 0;
                }
                break;
            default:
                break;
        }
    }
    public void SaveSpaceMonsterGame()
    {
        GameData gameData = new GameData()
        {
            Point = SpaceCraft.Point,
            SpaceCraftHealthPoint = SpaceCraft.HealthPoint,
            Amo = Shoot.AmmoName,
            AmoLevel = Shoot.Level,
            GameLevel = GenerateEnemy.Level
        };
        SaveSystem.SaveGame(gameData);
        Debug.Log(gameData.Point);
        Debug.Log(gameData.SpaceCraftHealthPoint);
        Debug.Log(gameData.Amo);
        Debug.Log(gameData.AmoLevel);
        Debug.Log(gameData.GameLevel);
    }
    public void DisplayLevelText()
    {
        EnableCanvasLevel(Level);
        Invoke("DisableCanvasLevel", 3);
        onetime = true;
    }
    public void EnableCanvasLevel(string Level)
    {
        var CanvasGameover = GameObject.Find("CanvasLevel").GetComponent<Canvas>();
        var LevelText = GameObject.Find("LevelText").GetComponent<Text>().text = Level;
        CanvasGameover.GetComponent<Canvas>().enabled = true;
    }
    public void DisableCanvasLevel()
    {
        var CanvasGameover = GameObject.Find("CanvasLevel").GetComponent<Canvas>();
        CanvasGameover.GetComponent<Canvas>().enabled = false;
    }
    public void SpawnEnemy(List<GameObject> listEnemy) {
        //khoang cach 
        var wanted = Random.Range(minTras, maxTras);
        //vi tri cua con quai vat minh random ra
        var position = new Vector3(wanted, transform.position.y);
        var enemy = RandomGameObject(listEnemy);
        //GEN QUAI VAT
        Instantiate(enemy, position, Quaternion.identity);
    }
    public GameObject RandomGameObject(List<GameObject> listEnemy) {
        int randomGameObjectIndex = Random.Range(0, listEnemy.Count);
        return listEnemy[randomGameObjectIndex];
    }
}
