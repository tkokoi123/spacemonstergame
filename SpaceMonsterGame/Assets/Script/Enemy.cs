using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    public GameObject explode;
    [SerializeField]
    public GameObject boss;
    public static int point;
    public Transform ViTriBoss;
    public static bool bossExist = true;
    public int bossHP;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Contains("DestroyAmmo"))
        {
            SpaceCraft.Point++;
            CommonFunction.SetTextPoint(SpaceCraft.Point); 
            Instantiate(explode, transform.position, Quaternion.identity);
            AudiosManager.Instance.PlaySFX("bomb");
            Destroy(gameObject);
        }
    }
    public void SetTextPoint(int point) {
        Text txtPoint = GameObject.Find("Point").GetComponent<Text>();
        txtPoint.text = $"Point: {point}";
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
