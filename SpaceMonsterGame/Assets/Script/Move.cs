using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Move : MonoBehaviour
{
    public float speed = 5;
    public float tiltAngle = 50;
    float minRangeX = -9.3f;
    float maxRangeX = 9.3f;
    float minRangeY = -3.9f;
    float maxRangeY = 3.9f;
    public Rigidbody2D rb;
    Vector3 PlayerPosition;
    private float movementHorizontal;
    private float movementVertical;

    // Start is called before the first frame update
    void Start()
    {
        //FindBoundaries();
        PlayerPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        movementHorizontal = Input.GetAxisRaw("Horizontal");
        movementVertical = Input.GetAxisRaw("Vertical");
        rb.velocity = new Vector2(movementHorizontal * speed, movementVertical * speed);
        if (movementHorizontal != 0)
        {
            float tiltAmount = Mathf.Clamp(rb.velocity.x * tiltAngle, -tiltAngle, tiltAngle);
            Quaternion targetRotation = Quaternion.Euler(0, tiltAmount, 0);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * 10);
        }
        else
        {
            // reset the player's rotation when not moving
            Quaternion targetRotation = Quaternion.Euler(0, 0, 0);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * 10);
        }
        Vector3 position = transform.position;
        position.x = Mathf.Clamp(position.x, minRangeX, maxRangeX);
        position.y = Mathf.Clamp(position.y, minRangeY, maxRangeY);
        transform.position = position;
    }
}

