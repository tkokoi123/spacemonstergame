using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShoot : MonoBehaviour
{

    public Transform gun;
    public GameObject ammo;
    public float speed;
    public float delay = 3;
    float timer;
    public bool Running { get; set; } = true;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (Running)
        {
            timer += Time.deltaTime;
            if (timer > delay)
            {
                SpawnEnemyAmmo();
                timer = 0;
            }
        }
    }
    public void SpawnEnemyAmmo()
    {
        var position = new Vector3(gun.transform.position.x, gun.transform.position.y -0.58f);
        var bullet = Instantiate(ammo, position, gun.transform.rotation);
        bullet.GetComponent<Rigidbody2D>().velocity = gun.up * speed;
    }
}
