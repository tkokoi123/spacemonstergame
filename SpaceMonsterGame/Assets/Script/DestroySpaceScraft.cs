using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DestroySpaceCraft : MonoBehaviour
{
    public GameObject CanvasObject;
    [SerializeField]
    public GameObject explode;
    [SerializeField]
    public GameObject TruMau;

    // Start is called before the first frame update
    void Start()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.Contains("Enemy") || collision.gameObject.name.Contains("Ammo_0")
            || collision.gameObject.name.Contains("Boss")|| collision.gameObject.name.Contains("DanCuaBoss")
            || collision.gameObject.name.Contains("bullet (58)_0") || collision.gameObject.name.Contains("bullet (81)_0"))
        {
            
            SpaceCraft.HealthPoint--;
            CommonFunction.SetHealthPoint();
            GameObject.Find("bomb").GetComponent<AudioSource>().Play();
            gameObject.GetComponent<Shoot>().Degrade();
            Instantiate(TruMau, transform.position, Quaternion.identity);
            if(!collision.gameObject.tag.Equals("BossLevel2")&& !collision.gameObject.tag.Equals("Boss"))
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.name.Contains("BigMonster_0")) {
            SpaceCraft.HealthPoint = 0;
        }
        if (collision.gameObject.tag.Equals("AsteroidLevel2"))
        {
            SpaceCraft.HealthPoint -= AsteroidLevel2.Damage;
            CommonFunction.SetHealthPoint();
        }
        if (collision.gameObject.name.Contains("MiniMeteor"))
        {
            SpaceCraft.HealthPoint--;
            CommonFunction.SetHealthPoint();
            GameObject.Find("bomb").GetComponent<AudioSource>().Play();
            Instantiate(TruMau, transform.position, Quaternion.identity);
            Destroy(collision.gameObject);
        }
    }
    // Update is called once per frame
    void Update()
    {
        Destroy();
    }
    public void Destroy()
    {
        Vector2 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
        if (screenPosition.y > Screen.height || screenPosition.y < 0 || screenPosition.x > Screen.width || screenPosition.x < 0)
        {
            Destroy(this.gameObject);
        }

    }
}
