﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Script
{
    public class AmmoManage : MonoBehaviour
    {
        private int damage = 1;
        public int Damage
        {
            get
            {
                if (gameObject.CompareTag("DestroyAmmo1")) return ShipAmmo1.Damage;
                else if (gameObject.CompareTag("DestroyAmmo2")) return ShipAmmo2.Damage;
                else return damage;
            }
            set
            {
                if (gameObject.CompareTag("DestroyAmmo1")) ShipAmmo1.Damage = value;
                else if (gameObject.CompareTag("DestroyAmmo2")) ShipAmmo2.Damage = value;
            }
        }
        private float speed = 3f;
        public float Speed
        {
            get
            {
                if (gameObject.CompareTag("DestroyAmmo1")) return ShipAmmo1.Speed;
                else if (gameObject.CompareTag("DestroyAmmo2")) return ShipAmmo2.Speed;
                else return speed;
            }
            set
            {
                if (gameObject.CompareTag("DestroyAmmo1")) ShipAmmo1.Speed = value;
                else if (gameObject.CompareTag("DestroyAmmo2")) ShipAmmo2.Speed = value;
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.name.Contains("Ammo_0"))
            {
                Destroy(collision.gameObject);
                Destroy(gameObject);
            }
            if (collision.gameObject.CompareTag("DestroyEnemy"))
            {
                //collision.gameObject.GetComponent<Enemy2_1>().GetDamage(Damage);
                Destroy(gameObject);
            }
        }
    }
}
