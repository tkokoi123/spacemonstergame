using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipAmmo1 : MonoBehaviour
{
    public static int Damage { get; set; } = 1;
    public static float Speed { get; set; } = 5f;
}
