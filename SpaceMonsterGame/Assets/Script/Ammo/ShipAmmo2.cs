using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipAmmo2 : MonoBehaviour
{
    public static int Damage { get; set; } = 3;
    public static float Speed { get; set; } = 3f;
}
