using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueAmo : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("DestroyEnemy")
           || collision.gameObject.name.Contains("Ammo_0"))
        {
            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
        if (collision.gameObject.name.Contains("Boss"))
        {
            Destroy(gameObject);
        }
    }
    void Update()
    {
        
    }
}
