using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CommonFunction : MonoBehaviour
{
    

    [SerializeField]
    GameObject Boss;
    public void ResetTheGame()
    {
        GameController.OnChangHpExist = false;
        AudiosManager.Instance.PlaySFX("click");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        SpaceCraft.Point = 0;
        SpaceCraft.HealthPoint = 10;
        Time.timeScale = 1;
        GenerateEnemy.Level = "Level1";
        GenerateEnemy.LevelNumber = 1;
        GameObject.FindGameObjectWithTag("CanvasGameover").GetComponent<Canvas>().enabled = false;
        GameObject.Find("CanvasWin").GetComponent<Canvas>().enabled = false;
    }
    public void StartSpaceMonster()
    {
        AudiosManager.Instance.PlaySFX("click");
        SceneManager.LoadScene("SampleScenee");
    }
    public void Guidance()
    {
        AudiosManager.Instance.PlaySFX("click");
        SceneManager.LoadScene("Guidance");
    }
    public void Guidance2()
    {
        AudiosManager.Instance.PlaySFX("click");
        SceneManager.LoadScene("Guidance2");
    }
    public void Back()
    {
        AudiosManager.Instance.PlaySFX("click");
        SceneManager.LoadScene("Menu");
    }
    public void QuitGame()
    {
        AudiosManager.Instance.PlaySFX("click");
        UnityEditor.EditorApplication.isPlaying = false;
    }
    public void MuteAllSound()
    {
        AudioListener.volume = 0;
    }

    public void UnMuteAllSound()
    {
        AudioListener.volume = 1;
    }


    public void SpawnEnemy()
    {
        Instantiate(Boss, transform.position, Quaternion.identity);
    }

    public static void SetTextPoint(int point)
    {
        Text txtPoint = GameObject.FindGameObjectWithTag("Point").GetComponent<Text>();
        txtPoint.text = $"Point: {point}";
    }

    public static void SetHealthPoint()
    {
        Text healthPoint = GameObject.FindGameObjectWithTag("HealthPoint").GetComponent<Text>();
        healthPoint.text = $"x{SpaceCraft.HealthPoint}";
    }
    public void SaveGame()
    {
        try
        {
        //set value for game data 
        GameData gameData = new GameData();
        SaveSystem.SaveGame(gameData);
        }
        catch
        {
            Debug.Log("Save error");
        }
    }
    public void LoadGame()
    {
        AudiosManager.Instance.PlaySFX("click");
        SceneManager.LoadScene("SampleScenee");
        var gameData = SaveSystem.LoadGame();
            Debug.Log(gameData.AmoLevel);
            Debug.Log(gameData.GameLevel);
            Debug.Log(gameData.Amo);
            Debug.Log(gameData.Point);
            Debug.Log(gameData.SpaceCraftHealthPoint);
            SpaceCraft.Point = gameData.Point;
            SpaceCraft.HealthPoint = gameData.SpaceCraftHealthPoint;
            GenerateEnemy.Level = gameData.GameLevel;
            GenerateEnemy.LevelNumber = Int32.Parse(gameData.GameLevel.Substring(4));
        Shoot.AmmoName= gameData.Amo;
        Shoot.Level= gameData.AmoLevel;
    }
}
