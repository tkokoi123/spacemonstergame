using Assets.Script;
using Mono.Cecil.Cil;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shoot : MonoBehaviour
{
    GameObject blueAmo;
    GameObject rocketAmo;
    public Transform gun;

    // tham so truyen vao trong qua trinh choi

    public static string AmmoName = "Lazer";
    public static float Level = 1;

    //loai dan
    public GameObject ammo;
    //toc do ban
    public float delayShoot;
    //level dan
    public float level = 1;

    public float maxLevel = 3;
    public float minLevel = 1;
    private void Update()
    {
        Level = level;
        AmmoName = (ammo.tag == "DestroyAmmo1" ? "Lazer" : "Rocket");
        GameObject.Find("BulletType").GetComponent<Text>().text = AmmoName;
    }

    void Start()
    {
        rocketAmo = Resources.Load<GameObject>("RocketAmmoRemovebg");
        blueAmo = Resources.Load<GameObject>("BlueLazerAmmoRemovebg-_0 1");
        level = Level;
        if (AmmoName.Equals("Lazer"))
        {
            ammo = blueAmo;
        }
        else
        {
            ammo = rocketAmo;
        }
        Invoke();
    }
    public void Invoke()
    {
        CancelInvoke("Fire");
        //goi ham Fire sau delayShoot giay.
        InvokeRepeating("Fire", 0, delayShoot);
    }
    void Fire()
    {
        if (level == 1)
        {
            gun1();
        }
        else if (level == 2)
        {
            //Debug.Log("FAILLLLL!!!!!");
            if (ammo.CompareTag("DestroyAmmo1"))
            {
                gun2();
            }
            else if (ammo.CompareTag("DestroyAmmo2"))
            {
                gun1();
            }

        }
        else if (level == 3)
        {
            if (ammo.CompareTag("DestroyAmmo1"))
            {
                gun3();
            }
            else if (ammo.CompareTag("DestroyAmmo2"))
            {
                gun4();
            }

        }

    }
    public void SetAmmo(GameObject ammo)
    {
        if (this.ammo.CompareTag(ammo.tag)) Upgrade();
        if (ammo.CompareTag("DestroyAmmo1"))
        {
            AmmoName = "Lazer";
            if (level == 1) delayShoot = 0.5f;
            else if (level == 2) delayShoot = 0.5f;
            else if (level == 3) delayShoot = 0.35f;
        }
        else if (ammo.CompareTag("DestroyAmmo2"))
        {
            AmmoName = "Rocket";
            if (level == 1) delayShoot = 1.25f;
            else if (level == 2) delayShoot = 1f;
            else if (level == 3) delayShoot = 0.8f;
        }
        this.ammo = ammo;
        Invoke();
    }
    public void Upgrade()
    {
        level++;
        if (level > maxLevel) level = maxLevel;
    }
    public void Degrade()
    {
        level--;
        if (level <= 0) level = 1;
    }
    private void gun1()         //      "       |       "
    {
        var bullet = Instantiate(ammo, gun.transform.position, gun.transform.rotation);
        bullet.GetComponent<Rigidbody2D>().velocity = gun.up * ammo.GetComponent<AmmoManage>().Speed;
    }
    private void gun2()         //      "       ||      "
    {
        Vector3 bulletPosition = new Vector3(gun.transform.position.x + 0.25f, gun.transform.position.y, gun.transform.position.z);
        Vector3 bullet2Position = new Vector3(gun.transform.position.x - 0.25f, gun.transform.position.y, gun.transform.position.z);
        var bullet = Instantiate(ammo, bulletPosition, gun.transform.rotation);
        bullet.GetComponent<Rigidbody2D>().velocity = gun.up * ammo.GetComponent<AmmoManage>().Speed;
        var bullet2 = Instantiate(ammo, bullet2Position, gun.transform.rotation);
        bullet2.GetComponent<Rigidbody2D>().velocity = gun.up * ammo.GetComponent<AmmoManage>().Speed;
    }
    private void gun3()         //      "       \|/     "
    {
        //vi tri
        Vector3 bullet1Position = new Vector3(gun.transform.position.x - 0.25f, gun.transform.position.y, gun.transform.position.z);
        Vector3 bullet2Position = new Vector3(gun.transform.position.x, gun.transform.position.y, gun.transform.position.z);
        Vector3 bullet3Position = new Vector3(gun.transform.position.x + 0.25f, gun.transform.position.y, gun.transform.position.z);
        // goc xoay
        Quaternion bullet1Rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, transform.rotation.z + 15);
        Quaternion bullet3Rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, transform.rotation.z - 15);
        //khoi tao
        var bullet = Instantiate(ammo, bullet1Position, bullet1Rotation);
        var bullet2 = Instantiate(ammo, bullet2Position, gun.transform.rotation);
        var bullet3 = Instantiate(ammo, bullet3Position, bullet3Rotation);
        //Phong dan
        bullet.GetComponent<Rigidbody2D>().velocity = bullet1Rotation * (gun.up * ammo.GetComponent<AmmoManage>().Speed);
        bullet2.GetComponent<Rigidbody2D>().velocity = gun.up * ammo.GetComponent<AmmoManage>().Speed;
        bullet3.GetComponent<Rigidbody2D>().velocity = bullet3Rotation * (gun.up * ammo.GetComponent<AmmoManage>().Speed);
    }
    private void gun4()         //      "       \ /     "
    {
        //vi tri
        Vector3 bullet1Position = new Vector3(gun.transform.position.x - 0.25f, gun.transform.position.y, gun.transform.position.z);
        Vector3 bullet2Position = new Vector3(gun.transform.position.x + 0.25f, gun.transform.position.y, gun.transform.position.z);
        // goc xoay
        Quaternion bullet1Rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, transform.rotation.z + 15);
        Quaternion bullet2Rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, transform.rotation.z - 15);
        //khoi tao
        var bullet = Instantiate(ammo, bullet1Position, bullet1Rotation);
        var bullet2 = Instantiate(ammo, bullet2Position, bullet2Rotation);
        //Phong dan
        bullet.GetComponent<Rigidbody2D>().velocity = bullet1Rotation * (gun.up * ammo.GetComponent<AmmoManage>().Speed);
        bullet2.GetComponent<Rigidbody2D>().velocity = bullet2Rotation * (gun.up * ammo.GetComponent<AmmoManage>().Speed);
    }
}
