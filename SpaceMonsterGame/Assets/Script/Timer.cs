using UnityEngine;

public class Timer : MonoBehaviour
{
    // Start is called before the first frame update
    float totalSeconds = 0;
    float elapsedSeconds = 0;
    bool running = false;
    bool started = false;
    public bool Finished { get { return !running && started; } }
    public bool Running { get { return running; } }
    public float Duration
    {
        set
        {
            if (!running)
            {
                totalSeconds = value;
            }
        }
    }


    public void Start()
    {

    }

    // Update is called once per frame
    public void Update()
    {
        if (running)
        {
            elapsedSeconds += Time.deltaTime;
            if (elapsedSeconds >= totalSeconds)
            {
                running = false;
            }
        }
    }
    public void Run()
    {
        if (totalSeconds > 0)
        {
            running = true;
            started = true;
            elapsedSeconds = 0;
        }
    }
}
