using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    // Start is called before the first frame update
    private bool isPause = false;
    public GameObject canvas;
    bool isMuteSound;
    public Sprite UnMuteImg;
    public Sprite MuteImg;
    public Button EditSound;
    public Slider BossHP;
    public Image bgSlider;
    public GameObject HPBossCanvas;
    bool isShowHPFinalBoss;
    public static bool OnChangHpExist = false;
    void Start()
    {
        //BossLv2 = GetComponent<BossLevel2>();
    }

    // Update is called once per frame
    void Update()
    {
        if (OnChangHpExist) {
        OnChangeHP();
        }
        HandlePauseGame();
    }
    public void ClickPauseGame()
    {
        AudiosManager.Instance.PlaySFX("click");
        if (isPause)
        {
            ResumeGame();
            isPause = false;
        }
        else
        {
            PauseGame();
            isPause = true;
        }
    }
    void HandlePauseGame()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            if (isPause)
            {
                ResumeGame();
                isPause = false;
            }
            else
            {
                PauseGame();
                isPause = true;
            }

        }
    }
    public void PauseGame()
    {
        AudiosManager.Instance.PlaySFX("click");
        canvas.SetActive(true);
        Time.timeScale = 0;
        isPause = true;
    }
    public void ResumeGame()
    {
        AudiosManager.Instance.PlaySFX("click");
        canvas.SetActive(false);
        Time.timeScale = 1;
        isPause = false;

    }
    public void HandleSound()
    {
        AudiosManager.Instance.PlaySFX("click");
        var c = FindObjectOfType<CommonFunction>();
        
        if (isMuteSound)
        {
            EditSound.image.sprite = UnMuteImg;
            c.UnMuteAllSound();
            isMuteSound = false;
        }
        else
        {
            EditSound.image.sprite = MuteImg;
            c.MuteAllSound();
            isMuteSound = true;
        }
    }
    public void Exit()
    {
        AudiosManager.Instance.PlaySFX("click");
        var c = FindObjectOfType<CommonFunction>();
        c.QuitGame();
    }

    public void OnChangeHP()
    {

        if (!isShowHPFinalBoss)
        {
            var bossLv2 = GameObject.FindGameObjectWithTag("BossLevel2").GetComponent<BossLevel2>();
            if (bossLv2 != null)
            {
                if (bossLv2.health > 0)
                {
                    Debug.Log("test");
                    HPBossCanvas.SetActive(true);
                    SetUpSlider(bossLv2.health);
                }
                else
                {
                    isShowHPFinalBoss = true;
                    HPBossCanvas.SetActive(false);
                }
            }
            else
            {
                isShowHPFinalBoss = true;
                HPBossCanvas.SetActive(false);
            }
        }
        
        if (isShowHPFinalBoss)
        {
            var finalBoss = GameObject.FindGameObjectWithTag(" Boss").GetComponent<bigMonsterShoot>();
            Debug.Log(finalBoss);
            HPBossCanvas.SetActive(true);
            SetUpSlider(finalBoss.health);
        }
    }
    public void SetUpSlider(int hp)
    {
        if (hp == 100)
        {
            bgSlider.color = new Color(255, 255, 255, 0);
        }
        else
        {
            bgSlider.color = Color.white;
        }
        BossHP.value = hp;
        Debug.Log(hp);
        Debug.Log(bgSlider.color);
    }
}
