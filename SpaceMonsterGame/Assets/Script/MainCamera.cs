using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainCamera : MonoBehaviour
{
    public GameObject CanvasObjectGameover;
    public GameObject CanvasObjectWin;
    // Start is called before the first frame update
    void Start()
    {
        GameObject.FindGameObjectWithTag("CanvasGameover").GetComponent<Canvas>().enabled = false;
        GameObject.FindGameObjectWithTag("CanvasWin").GetComponent<Canvas>().enabled = false;
        GameObject.FindGameObjectWithTag("CanvasLevel").GetComponent<Canvas>().enabled = false;
        //GameObject.Find("CanvasLevel").GetComponent<Canvas>().enabled = false;
        //GameObject.Find("CanvasWin").GetComponent<Canvas>().enabled = false;
        //GameObject.Find("CanvasGameover").GetComponent<Canvas>().enabled = false;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
