using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterAutoMove : MonoBehaviour
{
    // Start is called before the first frame update
    static float moveLeftRight = -2f;
    private Rigidbody2D rb;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        rb.MovePosition(rb.position + new Vector2(moveLeftRight, 0f) * Time.deltaTime);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.Contains("MaxCovidMoveRight")) {
            moveLeftRight = -2f;
            Debug.Log("MaxCovidMoveRight");
        }
        if (collision.gameObject.name.Contains("MaxCovidMoveLeft")) {
            moveLeftRight = 2f;
            Debug.Log("MaxCovidMoveLeft"); 
        }
    }
}

