using System;
using UnityEngine;

public class AudiosManager : MonoBehaviour
{
    //[SerializeField]
    public AudioSource musicSource, sfxSource;
    //[SerializeField]
    public Sound[] musicSounds, sfxSounds;
    private static AudiosManager instance;
    public static AudiosManager Instance
    {
        get
        {
            if (instance)
                return instance;

            instance = FindObjectOfType<AudiosManager>();

            if (instance)
                return instance;

            return instance = new GameObject(nameof(AudiosManager), typeof(AudioSource))
                .AddComponent<AudiosManager>();
        }
    }


    private void Awake()
    {
        if (instance && instance != this)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;

        // lazy initialization of AudioSource component
        if (!sfxSource)
        {
            if (!TryGetComponent<AudioSource>(out sfxSource))
            {
                sfxSource = gameObject.AddComponent<AudioSource>();
            }
        }

        DontDestroyOnLoad(gameObject);
    }

    public void PlayMusic(string name)
    {
        Sound sound = Array.Find(musicSounds, x => x.name == name);

        if (sound == null)
        {
            Debug.Log("Not Found");
        }
        else
        {
            musicSource.clip = sound.clip;
            musicSource.Play();
        }
    }

    private void Start()
    {
        PlayMusic("Theme");
    }

    public void PlaySFX(string name)
    {
        Sound sound = Array.Find(sfxSounds, x => x.name == name);
        if (sound == null)
        {
            Debug.Log("Not Found");
        }
        else
        {
            sfxSource.PlayOneShot(sound.clip);
        }
    }

    public void PlaySound(AudioClip audioClip)
    {
        sfxSource.PlayOneShot(audioClip);
    }

    public void MusicVolume(float value)
    {
        musicSource.volume = value;
    }

    public void SfxVolume(float value)
    {
        sfxSource.volume = value;
    }

}
