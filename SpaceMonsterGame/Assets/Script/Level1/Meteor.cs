using UnityEngine;
using static UnityEditor.PlayerSettings;
using System.Collections.Generic;
using UnityEngine.UI;
using Assets.Script;

public class Meteor : MonoBehaviour
{
    public GameObject[] miniMeteor;
    public GameObject explode;
    public int Health { get; set; }
    const float Impulse = 4f;
    Rigidbody2D rb;

    private void Start()
    {
        Health = 3;
        rb = GetComponent<Rigidbody2D>();
        var ship = GameObject.Find("maybay2d_0");
        if(ship != null)
        {
        Vector2 direction = new Vector2(ship.transform.position.x - transform.position.x, ship.transform.position.y - transform.position.y);
        direction.Normalize();
        rb.AddForce(direction * Impulse, ForceMode2D.Impulse);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Contains("DestroyAmmo"))
        {
            if (collision.gameObject.tag.Contains("DestroyAmmo"))
                Health -= collision.gameObject.GetComponent<AmmoManage>().Damage;
            if (Health <= 0)
            {
                AudiosManager.Instance.PlaySFX("bomb");
                Destroy(gameObject);
                SpaceCraft.Point++;
                CommonFunction.SetTextPoint(SpaceCraft.Point);
                Instantiate(explode, transform.position, Quaternion.identity);
                for (int i = 0; i < 4; i++)
                {
                    GameObject miniAsteroid = Instantiate(miniMeteor[i], transform.position, Quaternion.identity);
                }
            }
        }
        if (collision.gameObject.name.Contains("maybay"))
        {
            SpaceCraft.HealthPoint--;
            CommonFunction.SetHealthPoint();
            AudiosManager.Instance.PlaySFX("bomb");
            Destroy(gameObject);
            SpaceCraft.Point++;
            CommonFunction.SetTextPoint(SpaceCraft.Point);
            Instantiate(explode, transform.position, Quaternion.identity);
            for (int i = 0; i < 2; i++)
            {
                GameObject miniAsteroid = Instantiate(miniMeteor[i], transform.position, Quaternion.identity);
            }
        }
    }
}
