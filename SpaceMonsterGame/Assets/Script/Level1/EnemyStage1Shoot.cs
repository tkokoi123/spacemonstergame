using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStage1Shoot : MonoBehaviour
{
    public Transform gun;
    public GameObject ammo;
    public float speed;
    public float delay;
    float timer;
    public bool Running { get; set; } = true;
    void Update()
    {
        if (Running)
        {
            timer += Time.deltaTime;
            if (timer > delay)
            {
                SpawnEnemyAmmo();
                timer = 0;
                delay = Random.Range(4f, 7f);
            }
        }
    }
    public void SpawnEnemyAmmo()
    {
        var position = new Vector3(gun.transform.position.x, gun.transform.position.y - 0.58f);
        var bullet = Instantiate(ammo, position, gun.transform.rotation);
        bullet.GetComponent<Rigidbody2D>().velocity = gun.up * speed;
    }
}
