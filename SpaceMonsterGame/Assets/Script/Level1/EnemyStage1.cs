using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyStage1 : MonoBehaviour
{
    [SerializeField]
    public GameObject explode;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Contains("DestroyAmmo"))
        {
                Instantiate(explode, transform.position, Quaternion.identity);
                AudiosManager.Instance.PlaySFX("bomb");
                Destroy(gameObject);
                SpaceCraft.Point++;
                CommonFunction.SetTextPoint(SpaceCraft.Point);
        }
    }
    public GameObject RandomGameObject(List<GameObject> list)
    {
        int index = Random.Range(0, list.Count);
        return list[index];
    }
}
