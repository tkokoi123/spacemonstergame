using Assets.Script;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class MiniMeteor : MonoBehaviour
{
    public float speed = 10;
    public GameObject explode;
    public int Health { get; set; }
    private Rigidbody2D rb;
    const float Impulse = 4f;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        Vector2 direction = new Vector2(Random.Range(-5, 3f), -4f).normalized;
        rb.AddForce(direction * Impulse, ForceMode2D.Impulse);
    }
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Contains("DestroyAmmo") || collision.gameObject.name.Equals("maybay2d_0"))
        {
            AudiosManager.Instance.PlaySFX("bomb");
            Destroy(gameObject);
            SpaceCraft.Point++;
            CommonFunction.SetTextPoint(SpaceCraft.Point);
            Instantiate(explode, transform.position, Quaternion.identity);
        }
    }
}
