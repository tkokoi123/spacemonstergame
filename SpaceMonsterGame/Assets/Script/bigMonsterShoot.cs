using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class bigMonsterShoot : MonoBehaviour
{
    public int health=200;
    public Transform gun;
    public List<GameObject> ammo;
    public List<GameObject> amo2;
    public float speed;
    public float delay = 0.3f;
    public float delayAmo2 = 0.2f;
    float timer;
    float timerAmo2;
    [SerializeField]
    public GameObject explode;
    public GameObject CanvasObject;

    Canvas CanvasWin;
    public bool Running { get; set; } = true;
    // Start is called before the first frame update
    void Start()
    {
        GameObject.Find("BackgroundAudio").GetComponent<AudioSource>().Stop();
        CanvasWin = GameObject.Find("CanvasWin").GetComponent<Canvas>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Running)
        {
            timer += Time.deltaTime;
            if (timer > delay)
            {
                SpawnEnemyAmmo();
                SpawnEnemyAmo2();
                timer = 0;
            }
            //timerAmo2 += Time.deltaTime;
            //if (timerAmo2 > delayAmo2) {
            //    SpawnEnemyAmo2();
            //    timerAmo2 = 0;
            //}
        }
    }
    public void SpawnEnemyAmmo()
    {
        var position = new Vector3(gun.transform.position.x, gun.transform.position.y - 2f);
        var bullet = Instantiate(ammo[Random.Range(0, ammo.Count)], position, gun.transform.rotation);
        //bullet.GetComponent<Rigidbody2D>().velocity = gun.up * speed;
    }
    public void SpawnEnemyAmo2()
    {
        var position = new Vector3(gun.transform.position.x, gun.transform.position.y - 2f);
        var bullet = Instantiate(amo2[Random.Range(0,amo2.Count)], position, Quaternion.identity);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.Contains("BlueLazerAmmoRemovebg-_0") 
            || collision.gameObject.name.Contains("RocketAmmoRemovebg") )
        {
            health-=ShipAmmo1.Damage+5;
            GameObject.Find("BossHP").GetComponent<Text>().text = $"BoosHP {health}";
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.name.Contains("RocketAmmoRemovebg"))
        {
            GameObject.Find("BossHP").GetComponent<Text>().text = $"BoosHP {health}";
            health -= ShipAmmo2.Damage;
            Destroy(collision.gameObject);
        }
        KillBoss();
    }
    public void KillBoss()
    {
            if (health <= 0)
            {
                GameObject.Find("PointWin").GetComponent<Text>().text = "Your point " + SpaceCraft.Point;
                Instantiate(explode, transform.position, Quaternion.identity);
                Instantiate(CanvasObject, CanvasObject.transform.position, Quaternion.identity);
                CanvasWin.GetComponent<Canvas>().enabled = true;
                CanvasWin.GetComponent<AudioSource>().PlayOneShot(AudioManagement.Win);
                CanvasWin.GetComponent<AudioSource>().PlayOneShot(AudioManagement.Bomb);
                Destroy(gameObject);
                Enemy.point = 0;
                Enemy.bossExist = true;
                Time.timeScale = 0;
                SaveSystem.RemoveFile();
                //SceneManager.LoadSceneAsync("Game_end");

        }

    }
}
