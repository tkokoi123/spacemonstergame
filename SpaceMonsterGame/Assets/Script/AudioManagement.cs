using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManagement : MonoBehaviour
{
   
    public static AudioClip GameoverAudio = Resources.Load<AudioClip>("GameoverAudio");
    public static AudioClip Bomb = Resources.Load<AudioClip>("bomb");
    public static AudioClip DarkSout = Resources.Load<AudioClip>("DarkSout");
    public static AudioClip Hit = Resources.Load<AudioClip>("Hit");
    public static AudioClip Mucsic_BG = Resources.Load<AudioClip>("Mucsic_BG");
    public static AudioClip Win = Resources.Load<AudioClip>("win");

    public AudioManagement()
    {
    }

    public void PlayOneShotWithAudioClip(AudioClip audioInput) {
        GetComponent<AudioSource>().PlayOneShot(audioInput);
    }
}
