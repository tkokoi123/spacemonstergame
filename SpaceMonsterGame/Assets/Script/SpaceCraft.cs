using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SpaceCraft : MonoBehaviour
{
    public static int Point { get; set; } = 0;
    public static int HealthPoint { get; set; } = 10;

    Canvas CanvasGameover;
    public GameObject Explosion; 
    // Start is called before the first frame update
   
    void Start()
    {
        
        CommonFunction.SetHealthPoint();
    }

    // Update is called once per frame
    void Update()
    {
        //game over
        if(HealthPoint <= 0)
        {
            DestroySpaceScraft();
        }
        CommonFunction.SetTextPoint(Point);
    }
    public void DestroySpaceScraft()
    {
        Destroy(gameObject);
        Instantiate(Explosion, transform.position, Quaternion.identity);
        Text txtPoint = GameObject.FindGameObjectWithTag("PointGameOver").GetComponent<Text>();
        txtPoint.text = $"Point: {SpaceCraft.Point}";
        GameObject.FindGameObjectWithTag("CanvasGameover").GetComponent<Canvas>().enabled = true;

    }
}
